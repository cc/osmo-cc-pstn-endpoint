
#include <osmocom/core/utils.h>
#include <osmocom/core/logging.h>
#include "categories.h"

/* All logging categories used by this project. */

struct log_info_cat log_categories[] = {
	[DLCC] = {
		.name = "DLCC",
		.description = "libosmo-cc CC Layer",
		.color = "\033[0;37m",
	},
	[DOPTIONS] = {
		.name = "DOPTIONS",
		.description = "config options",
		.color = "\033[0;33m",
	},
	[DJITTER] = {
		.name = "DJITTER",
		.description = "jitter buffer handling",
		.color = "\033[0;36m",
	},
	[DDSP] = {
		.name = "DDSP",
		.description = "digital signal processing",
		.color = "\033[0;31m",
	},
	[DPH] = {
		.name = "DPH",
		.description = "PH SAP socket interface",
		.color = "\033[0;33m",
	},
	[DTEL] = {
		.name = "DTEL",
		.description = "Telephone application",
		.color = "\033[1;34m",
	},
};

size_t log_categories_size = ARRAY_SIZE(log_categories);

