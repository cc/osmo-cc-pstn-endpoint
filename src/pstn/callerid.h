
#include "../libfsk/fsk.h"
#include "../libdtmf/dtmf_encode.h"

enum cid_state {
	CID_STATE_NULL = 0,
	CID_STATE_WAIT_DTAS,
	CID_STATE_SEND_DTAS,
	CID_STATE_WAIT_TE_ACK,
	CID_STATE_WAIT_FSK,
	CID_STATE_SEND_FSK,
	CID_STATE_WAIT_DTMF,
	CID_STATE_SEND_DTMF,
	CID_STATE_WAIT_END,
};

typedef struct callerid {
	/* settings */
	int samplerate;			/* sample rate to render output */
	char cw;			/* use off-hook caller ID */
	char use_dtmf;			/* if set, use DTMF instead of FSK, start digit given */

	/* play states */
	enum cid_state state;		/* current action to perform */
	int wait;			/* time to send silence until next tone is sent */

	/* frame */
	uint8_t data[1024];		/* buffer to hold frame data to be sent */
	int len;			/* bytes in buffer */
	int pos;			/* byte in buffer to play next */
	int bpos;			/* bit to play next, in case of FSK */
	int seize, mark;		/* bit counters for seizure and mark signal */

	/* tone generation */
	double dtas_sine[65536];	/* sine wave for both DT_AS frequencies at correct level */
	double dtas_phaseshift65536[2];	/* frequency of DT_AS */
	double dtas_phase65536[2];	/* current phase of DT_AS */
	int dt_as_count;		/* count samples of DT_AS */
	fsk_mod_t fsk;			/* modulator for FSK */
	dtmf_enc_t dtmf;		/* modulator for DTMF */
} callerid_t;

int callerid_init(callerid_t *cid, int samplerate, int bell, char dtmf);
void callerid_exit(callerid_t *cid);
int callerid_set(callerid_t *cid, int cw, int dt_as, const char *callerid, uint8_t caller_type, int use_date);
void callerid_te_ack(callerid_t *cid, char digit);
int callerid_send(callerid_t *cid, sample_t *samples, int length);

